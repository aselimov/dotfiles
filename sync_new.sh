#!/bin/bash

#This sets up symbolic links only for files which do not already exist in the correct spots

#First set up symlinks for all of the global files
for file in $HOME/dotfiles/scripts/*; do 
    file=$(echo ${file##*/})
    if [ -e "$HOME/bin/$file" ]; then
        continue 
    else
        echo "Adding $file from scripts to .local/bin"
        ln -s $HOME/dotfiles/scripts/$file $HOME/bin/$file
    fi
done

for config in $HOME/dotfiles/.config/*; do 
    config=$(echo ${config##*/})
    if [ -e "$HOME/.config/$config" ]; then 
        continue
    else
        echo "Adding $file from .config"
        ln -s $HOME/dotfiles/.config/$config $HOME/.config/$config
    fi
done

for file in $HOME/dotfiles/share/*; do 
    file=$(echo ${file##*/})
    if [ -e "$HOME/.local/share/$file" ]; then
        continue 
    else
        echo "Adding $file from share to .local/share"
        ln -s $HOME/dotfiles/share/$file $HOME/.local/share/$file
    fi
done
