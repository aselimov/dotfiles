#!/bin/bash

#This is an initialization function which sets up all configurations different programs. This will set up symbolic links
#to the .dotfiles repository which must be set in the home path.

#WARNING: THIS WILL DELETE ALL EXISTING CONFIGURATIONS

#First set up symlinks for all of the global files
dotpath=$(pwd)
echo $dotpath
for file in $dotpath/scripts/*; do 
    file=$(echo ${file##*/})
    echo $file
    if [ -e $HOME/bin/$file ]; then
        rm $HOME/bin/$file 
    elif [ -L $HOME/bin/$file ]; then
        rm $HOME/bin/$file 
    fi

    ln -s $dotpath/scripts/$file $HOME/bin/$file
done

for config in $dotpath/.config/*; do 
    config=$(echo ${config##*/})
    if [ -e $HOME/.config/$config ]; then 
        rm $HOME/.config/$config -rf
    elif [ -L $HOME/.config/$config ]; then 
        rm $HOME/.config/$config -rf
    fi
    ln -s $dotpath/.config/$config $HOME/.config/$config
done

for file in $dotpath/home/.bash*; do 
    file=$(echo ${file##*/})
    if [ -e $HOME/$file ]; then 
        rm $HOME/$file
    elif [ -L $HOME/$file ]; then 
        rm $HOME/$file
    fi
    ln -s $dotpath/home/$file $HOME/$file
done

for file in $dotpath/share/*; do 
    file=$(echo ${file##*/})
    if [ -e $HOME/.local/share/$file ]; then 
        rm $HOME/.local/share/$file
    elif [ -L $HOME/.local/share/$file ]; then 
        rm $HOME/.local/share/$file
    fi
    ln -s $dotpath/share/$file $HOME/.local/share/$file
done


rm $HOME/.pubsrc
rm $HOME/.git_prompt
rm $HOME/.newsboat
rm $HOME/.xinitrc
ln -s $dotpath/home/.pubsrc $HOME/.pubsrc
ln -s $dotpath/home/.git_prompt $HOME/.git_prompt
ln -s $dotpath/home/.newsboat $HOME/.newsboat
ln -s $dotpath/home/.xinitrc $HOME/.xinitrc

    
#for config in $HOME/.dotfiles/$HOSTNAME/*; do 
#    config=$(echo ${config##*/})
#    test $HOME/.dotfiles/$HOSTNAME/$config && rm -r $HOME/.config/$config
#    ln -s $HOME/.dotfiles/$HOSTNAME/$config $HOME/.config/$config
#done
