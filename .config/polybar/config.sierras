 
;==========================================================
;
;
;   ██████╗  ██████╗ ██╗  ██╗   ██╗██████╗  █████╗ ██████╗
;   ██╔══██╗██╔═══██╗██║  ╚██╗ ██╔╝██╔══██╗██╔══██╗██╔══██╗
;   ██████╔╝██║   ██║██║   ╚████╔╝ ██████╔╝███████║██████╔╝
;   ██╔═══╝ ██║   ██║██║    ╚██╔╝  ██╔══██╗██╔══██║██╔══██╗
;   ██║     ╚██████╔╝███████╗██║   ██████╔╝██║  ██║██║  ██║
;   ╚═╝      ╚═════╝ ╚══════╝╚═╝   ╚═════╝ ╚═╝  ╚═╝╚═╝  ╚═╝
;
;
;   To learn more about how to configure Polybar
;   go to https://github.com/polybar/polybar
;
;   The README contains a lot of information
;
;==========================================================

[colors]

background = #282828
foreground = #928374
alert      = #afb4934 
focus      = #83a598
one        = #83a598
two        = #8ec07c
three      = #b8bb26
four       = #fabd2f 
five       = #fb4934 
#foreground = ${xrdb:foreground:#fff}
#background = ${xrdb:background:#000}

;  _                    
; | |__   __ _ _ __ ___ 
; | '_ \ / _` | '__/ __|
; | |_) | (_| | |  \__ \
; |_.__/ \__,_|_|  |___/

[bar/bar]
enable-ipc=true
height = 25 
width = 100%
offset-x = 0%
padding = 1
;bottom = true
radius-bottom = 0

background = ${colors.background}
;background = ${colors.primary}
foreground = ${colors.foreground}

alert      = ${colors.alert} 
focus      = ${colors.focus}
;border-size = 10
border-top-size = 0
border-bottom-size = 0
border-color = #0000

line-size = 1

font-0=Iosevka Nerd Font:size=13;6
font-1=MesloLGS NF:style=Regular:size=12;2
font-2=Material Icons:size=11;4
font-3=FontAwesome:size=11;4
font-4=Source Han Sans JP,源ノ角ゴシック JP,Source Han Sans JP Normal,源ノ角ゴシック JP Normal:size=14;5

modules-center = mpd
modules-right = wlan pulseaudio battery cpu-graph memory disk time
modules-left =dwm 

; bspwm
override-redirect = false


;                      _       _           
;  _ __ ___   ___   __| |_   _| | ___  ___ 
; | '_ ` _ \ / _ \ / _` | | | | |/ _ \/ __|
; | | | | | | (_) | (_| | |_| | |  __/\__ \
; |_| |_| |_|\___/ \__,_|\__,_|_|\___||___/
                                         
[module/updates]
type=custom/script
exec = yay -Qu | wc -l
exec-if = [[ $(yay -Qu | wc -l) != 0 ]]
format =   <label> update(s) available.
format-margin = 1
tail = true
interval = 30
click-left = urxvt -e yay -Syu

[module/wlan]
type = internal/network
interface = wlp2s0
interval = 3.0
spacing = 4
label_padding=1
format-connected =  <ramp-signal> <label-connected>
label-connected = %essid% 
label-connected-color1 = ${colors.color1}

format-disconnected = <label-disconnected>
label-disconnected = %ifname% disconnected
label-disconnected-color1 = ${colors.color1}


ramp-signal-0 = .
ramp-signal-0-foreground = ${colors.focus}
ramp-signal-1 = : 
ramp-signal-1-foreground = ${colors.focus}
ramp-signal-2 = ⋮
ramp-signal-2-foreground = ${colors.focus}
ramp-signal-3 = ⁞
ramp-signal-3-foreground = ${colors.focus}
format-margin=1

[module/time]
type = internal/date
interval = 10
format-margin = 1

time = "%H:%M"
date = "%b %d"

label = %date% %time% 
label-background = ${colors.background}
label-padding = 1
format-prefix = " "
format-prefix-foreground = ${colors.focus}

[module/pulseaudio]
type = internal/pulseaudio

format-volume-margin = 1
format-volume = <ramp-volume><label-volume>
label-volume = %percentage:4:4%%
format-volume-background = ${colors.background}
format-volume-padding = 1
use-ui-max = false
interval = 5

ramp-volume-0 = ""
ramp-volume-0-foreground = ${colors.focus}
ramp-volume-1 = "墳"
ramp-volume-1-foreground = ${colors.focus}
ramp-volume-2 = ""
ramp-volume-2-foreground = ${colors.focus}


label-muted = "婢 muted"   
label-muted-background = ${colors.background}
label-muted-padding = 1

[module/mpd]
type = internal/mpd
host = 127.0.0.1
port = 6600
interval = 2
format-online = <label-song> <label-time> <bar-progress>
format-paused = 
label-song = %artist% - %title%
bar-progress-width = 10 
bar-progress-indicator = | 
bar-progress-indicator-foreground = ${colors.focus}
bar-progress-fill = ─
bar-progress-fill-foreground = ${colors.focus}
bar-progress-empty = ─

[module/temperature]
type = internal/temperature
thermal-zone = 0
warn-temperature = 60

format =  <label>
format-warn = <ramp> <label-warn>

label = %temperature-c%
label-warn = %temperature-c%
label-warn-color1 = ${colors.secondary}

ramp-0 = 
ramp-1 = 
ramp-2 = 

[module/powermenu]
type = custom/menu

expand-right = false

format-margin = 2
format-spacing = 2

label-open = 
label-close = 
label-separator = |

; reboot
menu-0-1 = 
menu-0-1-exec = menu-open-2
; poweroff
menu-0-2 = 
menu-0-2-exec = menu-open-3
; logout
menu-0-0 = 
menu-0-0-exec = menu-open-1

menu-2-0 = 
menu-2-0-exec = reboot

menu-3-0 = 
menu-3-0-exec = poweroff

menu-1-0 = 
menu-1-0-exec = i3-msg exit || openbox --exit

[module/spotify]
type = custom/script
label-maxlen=30
format-margin = 0.5
format =    <label>
exec = playerctl metadata --format '{{artist}}: {{title}}'
tail = true
interval = 1
click-left = "playerctl play-pause"

[module/disk]
type=custom/script
format-margin =0.5
format = <label>
format-prefix =" "
format-prefix-foreground = ${colors.focus}
exec = "echo 1: $(df -h /dev/sda3 | tail -n1 |awk '{print $5}')"
tail = true
interval = 1

[module/dwm]
type = internal/dwm
format = <label-tags> <label-layout> <label-floating>
; Path to dwm socket (default: /tmp/dwm.sock)
socket-path = /tmp/dwm.sock

; Left-click to view tag, right-click to toggle tag view
enable-tags-click = false
; Scroll to cycle between available tags
enable-tags-scroll = false
; If true and enable-tags-scroll = true, scrolling will view all tags regardless if occupied
tags-scroll-empty = false
; If true and enable-tags-scroll = true, scrolling will cycle through tags backwards
tags-scroll-reverse = false
; If true and enable-tags-scroll = true, wrap active tag when scrolling
tags-scroll-wrap = false
; Left-click to set secondary layout, right-click to switch to previous layout
enable-layout-click = false
; Scroll to cycle between available layouts
enable-layout-scroll = false
; Wrap when scrolling and reaching beginning/end of layouts
layout-scroll-wrap = false
; Reverse scroll direction
layout-scroll-reverse = false

; If enable-layout-click = true, clicking the layout symbol will switch to this layout
secondary-layout-symbol = [M]

; Separator in between shown tags
; label-separator = |

; Title of currently focused window
; Available tokens:
;   %title%
label-title = %title%
label-title-padding = 1
label-title-foreground = ${colors.foreground}
label-title-maxlen = 30

; Symbol of current layout
; Available tokens:
;   %symbol%
label-layout = %symbol%
label-layout-padding = 1
label-layout-foreground = ${colors.foreground} 
label-layout-background = ${colors.background}

; Text to show when currently focused window is floating
label-floating = F

; States: focused, unfocused, visible, urgent, empty
; Available tokens:
;   %name%

; focused = Selected tag on focused monitor
label-focused = %name%
label-focused-background = ${colors.focus}
label-focused-underline= ${colors.focus}
label-focused-foreground = ${colors.background}
label-focused-padding = 1

; unfocused = Unselected tag on unselected monitor
label-unfocused = %name%
label-unfocused-padding = 1

; visible = Unselected tag, but occupied tag on any monitor
label-visible = %name%
label-visible-background = ${colors.background}
label-visible-underline = ${self.label-focused-underline}
label-visible-padding = ${self.label-focused-padding}

; urgent = Unselected tag with window that has urgency hint set
label-urgent = %name%
label-urgent-background = ${colors.alert}
label-urgent-padding = 1

; empty = Unselected and unoccupied tag
; This can be set to an empty string to hide empty tags
label-empty = %name%
label-empty-background = ${colors.background}
label-empty-padding = 1

[module/filesystem]
type = internal/fs
filesystem-type = HDD
spacing = 4
label_padding=4
; Mountpoints to display
mount-0 = /
mount-1 = /mnt/DATA
; Seconds to sleep between updates
; Default: 30
interval = 10
; Display fixed precision values
; Default: false
fixed-values = true
; Spacing between entries
; Default: 2
format-mounted=<label-mounted>
label-mounted = %mountpoint%:%percentage_used% %

[module/memory]
type = internal/memory
interval = 0.1
format-prefix = " "
format-prefix-foreground = ${colors.focus}
#format-underline = ${colors.focus}
;format-margin = ${margin.for-modules}
format-padding = 1
label = "%percentage_used:2%%"
format-margin=1

[module/cpu-graph]
type = internal/cpu
interval = 0.5
format = <label>
label = %percentage-cores%
format-prefix = " "
format-prefix-foreground=${colors.focus}
format-margin = 0.5
format-padding = 1
ramp-coreload-0 = ▁
ramp-coreload-0-foreground = ${colors.one}
ramp-coreload-1 = ▂
ramp-coreload-1-foreground = ${colors.two}
ramp-coreload-2 = ▃
ramp-coreload-2-foreground = ${colors.three}
ramp-coreload-3 = ▄
ramp-coreload-3-foreground = ${colors.three}
ramp-coreload-4 = ▅
ramp-coreload-4-foreground = ${colors.four}
ramp-coreload-5 = ▆
ramp-coreload-5-foreground = ${colors.four}
ramp-coreload-6 = ▇
ramp-coreload-6-foreground = ${colors.five}
ramp-coreload-7 = █
ramp-coreload-7-foreground = ${colors.five}
ramp-coreload-font = 3

[module/battery]
type = internal/battery
full-at = 99
battery = BAT0
adapter = ADP0
format-charging = <animation-charging> <label-charging>
;format-charging-background = ${colors.shade2}
format-charging-padding = 1
label-charging = %percentage%%
format-discharging = <ramp-capacity> <label-discharging>
format-discharging-padding = 1
label-discharging = %percentage:2%%
format-full = <label-full>
format-full-padding = 1
format-charging-margin = 0.5
format-discharging-margin = 0.5
format-full-margin = 0.5
format-full-prefix = " "
format-full-prefix-foreground = ${colors.three}
ramp-capacity-0 = 
ramp-capacity-1 = 
ramp-capacity-2 = 
ramp-capacity-3 = 
ramp-capacity-4 = 
ramp-capacity-5 = 
ramp-capacity-6 = 
ramp-capacity-7 = 
ramp-capacity-8 = 
ramp-capacity-9 = 
ramp-capacity-0-foreground = ${colors.five}
ramp-capacity-1-foreground = ${colors.five}
ramp-capacity-2-foreground = ${colors.five}
ramp-capacity-3-foreground = ${colors.five}
ramp-capacity-4-foreground = ${colors.four}
ramp-capacity-5-foreground = ${colors.four}
ramp-capacity-6-foreground = ${colors.three}
ramp-capacity-7-foreground = ${colors.three}
ramp-capacity-8-foreground = ${colors.three}
ramp-capacity-9-foreground = ${colors.three}
animation-charging-0 = 
animation-charging-1 = 
animation-charging-2 = 
animation-charging-3 = 
animation-charging-4 = 
animation-charging-5 = 
animation-charging-6 = 
animation-charging-foreground = ${colors.foreground}
animation-charging-framerate = 750

[settings]
screenchange-reload = true
;compositing-background = xor
;compositing-background = screen
;compositing-foreground = source
;compositing-border = over
pseudo-transparency = false

[global/wm]
margin-top = 0
margin-bottom = 0

; vim:ft=dosini
