#!/bin/bash

# Terminate already running bar instances
killall -q polybar

# Wait until the processes have been shut down
while pgrep -u $UID -x polybar >/dev/null; do sleep 1; done

if [ $HOSTNAME = 'laptop' ]; then 
    polybar -r bar -c $HOME/.config/polybar/config.laptop & 
elif [ $HOSTNAME = 'desktop' ]; then 
    polybar -r bar -c $HOME/.config/polybar/config.desktop & 
elif [ $HOSTNAME = 'sierras' ]; then 
    polybar -r bar -c $HOME/.config/polybar/config.sierras & 
fi

echo "Polybar launched..."
