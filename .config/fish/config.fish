set fish_greeting ""
alias vi="nvim"
alias vim="nvim"
alias rs="rsync -zv --partial --progress"
alias mergepdf="gs -dBATCH -dNOPAUSE -dQUIET -sDEVICE=pdfwrite -sOutputFile=output.pdf"
#alias sxiv="sxiv-rifle"
set PATH $PATH /home/aselimov/.local/bin
starship init fish | source
#mw -Y > /dev/null 2>&1 &
set -x OMPI_MCA_rmaps_base_oversubscribe 1
