function get_tot_stress
set stress (cacmb --calc $argv[1] tot_virial -set_types 2 Ni Cu | tail -n1)
set time (echo $argv[1] | egrep -o [0-9]+)
echo $time $stress >> $argv[2]
end
