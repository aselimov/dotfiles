map <C-b> : w % <Bar> ! python % <CR>
nnoremap ;c : IPythonCellExecuteCellJump <CR>
nnoremap ;C : IPythonCellExecuteCell <CR>
nnoremap ;r : w % <Bar> IPythonCellRun<CR>
nnoremap ;t : T ipython <CR>
