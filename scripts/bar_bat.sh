#!/bin/bash

#First we get the capacity
charge=$(cat /sys/class/power_supply/BAT0/capacity)

#Now get the status 
bstat=$(cat /sys/class/power_supply/BAT0/status)

#Get the symbol for the capacity
if [ "$charge" -gt 90 ]; then 
    bat=
elif [ "$charge" -gt 70 ]; then 
    bat=
elif [ "$charge" -gt 50 ]; then 
    bat=
elif [ "$charge" -gt 30 ]; then 
    bat=
elif [ "$charge" -gt 10 ]; then 
    bat=
else
    bat=
fi


if [ "$bstat" = "Discharging" ]; then 
    cstat="-"
elif [ "$bstat" = "Charging" ]; then 
    cstat="+"
else
    cstat="="
fi

echo "$cstat$bat $charge%"

