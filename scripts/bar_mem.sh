#!/bin/bash

free=$(grep -oP '^MemFree: *\K[0-9]+' /proc/meminfo)
total=$(grep -oP '^MemTotal: *\K[0-9]+' /proc/meminfo)
mem=$(echo "scale=1; 100*($total-$free)/$total"| bc | cut -d '.' -f1 )

echo " $mem%"
