#!/bin/bash

#This script just starts a directory set up for a latex presentation of the desired kind. 
# The first argument for this script is the project name and the second is the project type 

function fail {
    printf '%s\n' "$1" >&2
    exit "${2-1}"
}

#Make the new project directory
xelatex $1 && biber ${1/.tex/} && xelatex $1
