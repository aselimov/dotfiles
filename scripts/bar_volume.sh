#!/usr/bin/bash
[ $(pamixer --get-mute) = true ] && echo 婢 && exit

vol="$(pamixer --get-volume)"

if [ "$vol" -gt "70" ]; then 
    icon="墳"
elif [ "$vol" -gt "30" ]; then 
    icon="奔"
else
    icon="奄"
fi

echo "$icon $vol%"
