#!/bin/bash

papesdir=$HOME/dotfiles/wallpapers
files=($papesdir/*)
count=$(find $papesdir -type f | wc -l)

while :; do
    index=$((($RANDOM % $count)))
    index2=$((($RANDOM % $count)))
    feh --bg-fill  ${files[$index]}
    sleep 15m
done
