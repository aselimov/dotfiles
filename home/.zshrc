# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt extendedglob notify
unsetopt autocd beep
bindkey -v
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/aselimov/.zshrc'

shuf -n 1 ~/dotfiles/scripts/kjv.txt
autoload -Uz compinit
compinit
# End of lines added by compinstall
#
export OMPI_MCA_rmaps_base_oversubscribe=1
export CLICOLOR=1
export LSCOLORS=ExGxBxDxCxEgEdxbxgxcxd
alias vi="nvim"
alias vim="nvim"
alias rs="rsync -zv --partial --progress"
alias mergepdf="gs -dBATCH -dNOPAUSE -dQUIET -sDEVICE=pdfwrite -sOutputFile=output.pdf"
alias ddg="w3m ddg.gg"
#alias sxiv="sxiv-rifle"
function addbin(){
    ln -s $PWD/$1 /home/aselimov/bin
}
eval "$(starship init zsh)"
zstyle -e ':completion:*:hosts' hosts 'reply=(
  ${=${${(f)"$(cat {/etc/ssh_,~/.ssh/known_}hosts(|2)(N) 2>/dev/null)"}%%[#| ]*}//,/ }
  ${=${${${${(@M)${(f)"$(cat ~/.ssh/config 2>/dev/null)"}:#Host *}#Host }:#*\**}:#*\?*}}
)'

source "/home/aselimov/.config/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh"
source "/home/aselimov/.config/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh"
source "/home/aselimov/.config/zsh/zsh-history-substring-search/zsh-history-substring-search.zsh"

bindkey '^[[A' history-substring-search-up
bindkey '^[[B' history-substring-search-down
source /usr/share/undistract-me/long-running.bash
notify_when_long_running_commands_finish_install
